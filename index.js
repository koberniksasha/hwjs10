"use strict";

const tabs = document.querySelectorAll(".tabs-title");
const tabContents = document.querySelectorAll(".tabs-content li");

tabs.forEach((tab, index) => {
  tab.addEventListener("click", () => {
    tabs.forEach((tab) => tab.classList.remove("active"));
    tabContents.forEach((content) => (content.style.display = "none"));

    tab.classList.add("active");
    tabContents[index].style.display = "block";
  });
});
